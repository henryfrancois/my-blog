---
layout: "../../layouts/BlogPost.astro"
slug: "approche-technique-sur-la-realisation-dun-Design-System-part-1"
title: "Approche technique sur la réalisation d'un Design System - part 1"
description: "Je vous propose quelques points à avoir à l'esprit pour la réalisation d'un Design System"
pubDate: "Jul 08 2022"
heroImage: "/cover-ds-part-1.jpg"
---

_disclaimer : cet article n'est pas un tutoriel, mais plutôt un condensé de partage d'expérience et de conseils / bonnes pratiques_

Hop hop hop Moussaillon ! Lève les mains de ton clavier !!! On ne commence pas un design system en codant mais en se posant LA bonne question.

## Quelle est ma cible ?

Pour être plus clair, aujourd'hui les frameworks qui dominent le marché sont Angular, React, Vue. Donc quelle(s) est/sont votre/vos cible(s) ?

Un seul des trois ? Les trois mon capitaine !

Dans cet article nous allons aborder le cas de figure où vous travaillez pour une grande société. Et cette société n'a pas forcé ses équipes projet à utiliser un seul framework. De ce fait, il y a un certain nombre d'applications existantes dans différents frameworks. Vous souhaitez développer une librairie de composants qui puisse être adoptée par l'ensemble de ces applications, ainsi que toutes les futures applications. C'est pour cela que nous n'allons pas cibler seulement un framework.

Heureusement pour vous il y a des solutions !

## Les web components

Afin d'être compatible avec l'ensemble des frameworks web, y compris ceux qui n'ont pas été cités plus haut, vous devez créer des "web components".

Ce que je veux dire par là, c'est que si vous réalisez un composant en Angular, il n'est évidemment pas utilisable en React. Et dans n'importe quelle combinaison Angular, React, Vue, on tombe sur le même constat.

En revanche tous les frameworks sont compatibles avec les web components.

## C'est quoi un web component ?

Pour une fois en informatique, c'est très simple à comprendre ! 😁

Voici un ensemble de web components que vous connaissez (liste non exhaustive) :

```HTML
<div>ceci est un web component</div>
<h1>ceci est un web component</h1>
<span>ceci est un web component<span>
<ul>
  ceci est un web component
  <li>ceci est un web component</li>
</ul>
```

Oui oui, vous comprenez bien ce qui est écrit... Tous les éléments HTML natifs sont des web components.

Et il existe des API pour créer nos propres composants web. Donc vous allez créer vos propres éléments HTML. C'est cool non ? 😁

## On commence à rentrer dans la technique

Vous pouvez créer vos propres éléments HTML à la main, en javascript. Si vous avez envie de vous faire mal, c'est possible. [(voir documentation officielle)](https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements)

Mais il existe des frameworks / librairies pour le faire. Et moi je n'aime pas me faire mal...

Donc nous allons utiliser [Stencil JS](https://stenciljs.com/) . Pourquoi ce choix ?

1. Parce qu'avec le même code, on peut avoir plusieurs cibles (Angular, React, Vue, Web components, ...)
2. Parce que l'output de Stencil, c'est une librairie de composants qui est facile à fournir à nos utilisateurs
3. Dernier point, je viens juste de le dire, j'aime pas me faire mal... Faut suivre un peu !

<img src="/map.jpg" style="width:480px;">

## Comment ça plusieurs cibles ?

`Moussaillon, tant qu'il y a du vent, on peut visiter l'ensemble des mers !`

Comme on l'a dit précédemment, l'avantage majeur des web components, c'est qu'ils sont compatibles avec tous les frameworks. Sauf que c'est aussi leur plus gros défaut... 😔

Les web components suivent l'API standard du HTML. C'est-à-dire que pour passer de la data à un composant, il faut la lui donner à travers un attribut standard, donc via une `string`.

```HTML
<my-component my-attribute="je ne peux passer que des chaînes de caractères...">
</my-component>
```

Hors, à travers les `props` de nos composants, on passe souvent des données structurées, des tableaux ou des objets.

Cela a pour conséquence de ne pas pouvoir concevoir des API de composants trop complexes.

Grâce à Stencil, nous allons pouvoir transpiler notre code dans autant de cibles que l'on souhaite : Angular, React, Vue, Web components... Ainsi, nous allons pouvoir retrouver la possibilité de transmettre des données structurées de manière plus complexe qu'en passant par une `string`.

En gros, Stencil est capable de `wrapper` les web components dans des composants "natifs" de votre framework préféré.
Regardez la documentation de Stencil pour savoir s'il est bien supporté.

> **Note** : Au moment d'écrire cet article, Stencil est capable de wrapper les composants dans les trois principaux frameworks (Angular, React et Vue).

Au final vous auriez quatre librairies, puisque vous garderiez la librairie de web components pour les cas qui sortent des trois frameworks principaux. Quatre librairies à mettre à disposition des développeurs de votre entreprise, afin qu'ils commencent de nouveaux projets en intégrant votre design system ou qu'ils puissent faire la transition au rythme de leur développement pour les applications déjà existantes.

## La suite au prochain épisode

Je vous laisse regarder la documentation de Stencil pour commencer à setup votre projet en fonction de vos cibles.
On se retrouve dans le prochain épisode pour quelques conseils et bonnes pratiques sur vos composants.

Bon voyage moussaillons !

<img src="/team-pirate.jpg" style="width:640px;">
