---
layout: "../layouts/About.astro"
title: "About Me"
description: "Lorem ipsum dolor sit amet"
heroImage: "/placeholder-about.jpg"
---

Après 10 ans à travailler dans l’industrie en tant que Technicien de Maintenance et Technicien de production, à 30 ans j’ai décidé de me reconvertir dans l’informatique.

J’ai été accepté à l’école 42 Paris (promotion 2015). Je suis donc devenu développeur. Après un premier projet de création de startup en partenariat avec La Marine Nationale et Thalès (mi-2017 / mi-2018), j’ai signé mon premier CDI de consultant en juin 2018. Depuis novembre 2019 je travaille pour le groupe Talan et plus particulièrement Talan Labs. Lors de mes missions, j’ai pû travailler en équipe en méthode agile.

Lors de mes 2 premières missions en Big Data, j’ai découvert :

- Java 8
- Cassandra
- Elasticsearch / Kibana
- Kafka / Zookeeper
- Docker

Depuis 3 ans, je suis sur des projets d’application web plus classique :

- Node JS
- Vue JS
- Angular (niveau débutant)
- React (niveau débutant)
- Stencil JS
- PostgreSQL
- Docker
- Cypress
- Testing Library
- Azure (niveau débutant)

J’ai aussi eu à encadrer une équipe de dev, en tant que Lead Tech pendant 9 mois. J’ai recruté 5 dev pour agrandir une équipe et en créer une seconde. J’ai dû prendre des choix techniques sur les projets, accompagner leur lancement et suivre leur avancé. Ce fût une experience enrichissante, mais je préfère retourner a un poste de Lead Dev pour le moment.

En parallèle de ma mission, je suis devenu “manager” au sein de Talan en 2022. Je suis 5 dev de Talan lors de revu mensuel et d’entretien annuel.

Chez Talan Labs, un jour par mois, nous avons ce qu’on appelle la journée “ruche”. Nous dé facturons afin d’assister à des présentations d’autres “labiens” sur différents sujets qu’ils rencontrent en mission ou pas. J’ai pû moi-même participer en tant que “speaker” a ces journées à plusieurs reprisent. J’ai notamment présenté un sujet sur l’accessibilité web et un autre sur le découpage en composant d’une application front-end.

Si un jour vous cherchez à me recruter, sachez que je suis “king-size”. J’ai donc besoin d’un bureau et d’un bon siège. Pas d’un environnement “cozy”, “startup” où je devrais m’installer dans un canapé.

Et sachez que j’aimerais pouvoir utiliser mon propre clavier/souris. Car je travaille en QWERTY. Le remote a minima partiel doit être possible.

Dernier point, mes passions, j'ai tout lu d'Isaac Asimov et ne me lancez pas sur le sujet de la F1, sinon c'est parti pour des heures... A vos risques et périlles 🤣